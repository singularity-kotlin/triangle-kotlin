import java.lang.Math.*
import kotlin.math.sqrt

class Triangle(
    var p1: Point,
    var p2: Point,
    var p3: Point,
    ) {
    fun getSides(): DoubleArray {
        var sides = DoubleArray(3)
        sides[0] = sqrt(((p2.x-p1.x)*(p2.x-p1.x) + (p2.y-p1.y)*(p2.y-p1.y)).toDouble())
        sides[1] = sqrt((p3.x-p1.x)*(p3.x-p1.x) + (p3.y-p1.y)*(p3.y-p1.y).toDouble())
        sides[2] = sqrt((p3.x-p2.x)*(p3.x-p2.x) + (p3.y-p2.y)*(p3.y-p2.y).toDouble())
        return sides;
    }
    fun print() {
        println("Triangle(")
        for(side in getSides()) {
            println(String.format("side=%.1f", side))
        }
        println(")")
    }
    fun perimeter(): Double {
        var total=0.0
        for(side in getSides())
            total+=side
        return total
    }
    fun area(): Double {
        val p = perimeter()/2
        val sides = getSides()
        val a = sides[0]
        val b = sides[1]
        val c = sides[2]

        return sqrt(p*(p-a)*(p-b)*(p-c))
    }
}

class Point(public var x: Int, public var y: Int) {

}

fun main() {
    val triangle = Triangle(Point(0,0), Point(0,10), Point(4,0))
    triangle.print()
    println("Perimeter=${triangle.perimeter()}")
    println("Area=${triangle.area()}")
}